package com.linkroster.springjwt.controllers;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.linkroster.springjwt.models.ImageModel;
import com.linkroster.springjwt.payload.response.MessageResponse;
import com.linkroster.springjwt.repository.ImageRepository;





@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/Image")

public class ImageUploadController {

	@Autowired
	ImageRepository imageRepository;
	@PutMapping("/uploadImageData/{userId}")
	public ResponseEntity<?> uploadImage(@PathVariable(value = "userId") Long userId,@RequestParam("imageFile") MultipartFile file) throws IOException 
	{

		System.out.println("Original Image Byte Size - " + file.getBytes().length);
		ImageModel img = new ImageModel(userId,file.getOriginalFilename(), file.getContentType(),
				compressBytes(file.getBytes()));
//		img.setUserId(userId);
        imageRepository.save(img);
	
		return ResponseEntity.ok(new MessageResponse("profile pictured uploaded successfully"));
	}
	
	@GetMapping(path = { "/getImageData/{userId}" }) 
	public ImageModel getImage(@PathVariable("userId") Long userId) throws IOException {

		ImageModel retrievedImage = imageRepository.findByUserId(userId);
		
		ImageModel img = new ImageModel(userId,retrievedImage.getName(), retrievedImage.getType(),
				decompressBytes(retrievedImage.getPicByte()));

	return retrievedImage;
	}
	@PutMapping("/updateImageData/{userId}")
	public ResponseEntity<?> updateImageData(@PathVariable(value = "userId") Long userId,@RequestParam("imageFile") MultipartFile file) throws IOException 
	{
//		Optional<ImageModel> img1 = imageRepository.findByUserId(userId);
		ImageModel retrievedImage = imageRepository.findByUserId(userId);
		
		retrievedImage.setName(file.getOriginalFilename());
		retrievedImage.setType(file.getContentType());
		retrievedImage.setPicByte(compressBytes(file.getBytes()));
	
		ImageModel updateImage= imageRepository.save(retrievedImage);
			
			return ResponseEntity.ok(updateImage);


	}
	

	

	public static byte[] compressBytes(byte[] data) {
		Deflater deflater = new Deflater();
		deflater.setInput(data);
		deflater.finish();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		while (!deflater.finished()) {
			int count = deflater.deflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		try {
			outputStream.close();
		} catch (IOException e) {
		}
		System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);

		return outputStream.toByteArray();
	}

	// uncompress the image bytes before returning it to the angular application
	public static byte[] decompressBytes(byte[] data) {
		Inflater inflater = new Inflater();
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		try {
			while (!inflater.finished()) {
				int count = inflater.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
		} catch (IOException ioe) {
		} catch (DataFormatException e) {
		}
		return outputStream.toByteArray();
	}
}