package com.linkroster.springjwt.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.linkroster.springjwt.models.ImageModel;
import com.linkroster.springjwt.models.User;
import com.linkroster.springjwt.models.UserImage;
import com.linkroster.springjwt.payload.response.MessageResponse;
import com.linkroster.springjwt.repository.AmazonS3ClientService;
import com.linkroster.springjwt.repository.UserImageRepository;
import com.linkroster.springjwt.security.services.EmailService;

import com.linkroster.springjwt.service.AmazonS3ClientServiceImpl;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class UserimageController {

	@Autowired
	private JavaMailSender javaMailSender;
	@Autowired
	UserImageRepository userImageRepository;
	@Autowired
	private EmailService emailService;

	@Autowired
	private AmazonS3ClientService amazonS3ClientService;
	private static Random rand = new Random((new Date()).getTime());

	@PostMapping("/uploadimageFile")
	public Map<String, String> uploadimageFile(@RequestPart(value = "file") MultipartFile file) {
		this.amazonS3ClientService.uploadFileToS3Bucket(file, true);
		Map<String, String> response = new HashMap<>();
		response.put("message", "file [" + file.getOriginalFilename() + "] uploading request submitted successfully.");
		return response;
	}

	@PostMapping("/uploadimageFile/{userId}")
	public Map<String, String> uploadimageFile(@PathVariable(value = "userId") User userId,
			@RequestParam(value = "file") MultipartFile file) {
		this.amazonS3ClientService.uploadFileToS3Bucket(file, true);
		Map<String, String> response = new HashMap<>();
		response.put("message", "file [" + file.getOriginalFilename() + "] uploading request submitted successfully.");
		UserImage userfromDB;


		userfromDB =userImageRepository.findByUserId(userId.getId());
				
		

		if (userfromDB != null) {
			
		userfromDB.setName(file.getOriginalFilename());
		userImageRepository.save(userfromDB);

		

		} else

		{
		
		
		UserImage img = new UserImage(userId, file.getOriginalFilename(), file.getContentType());

		userImageRepository.save(img);}
		return response;
	}

	@GetMapping(path = { "/getUserImage/{userId}" })
	public UserImage getImage(@PathVariable("userId") Long userId) throws IOException {

		UserImage retrievedImage = userImageRepository.findByUserId(userId);
		
		return retrievedImage;
	}

	
	@GetMapping("/all")
	public String allAccess() {
		System.out.println("inside allAccess");
		return "Public Content. - Updated";
	}

	
	@GetMapping("/user")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public String userAccess() {
		return "User Content.";
	}

	@GetMapping("/mod")
	@PreAuthorize("hasRole('MODERATOR')")
	public String moderatorAccess() {
		return "Moderator Board.";
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return "Admin Board.";
	}

	@GetMapping("/email")
	public String sendEmail() {

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo("karthika.sankar1118@gmail.com", "karthika@greenitive.com");


		
		msg.setSubject("Testing from Spring Boot");
		msg.setText("Hello World \n Spring Boot Email");

		javaMailSender.send(msg);
		return "mail sent";

	}

	public static String encrypt(String str) {

		BASE64Encoder encoder = new BASE64Encoder();

		byte[] salt = new byte[8];

		rand.nextBytes(salt);

		return encoder.encode(salt) + encoder.encode(str.getBytes());
	}

	public static String decrypt(String encstr) {

		if (encstr.length() > 12) {

			String cipher = encstr.substring(12);

			BASE64Decoder decoder = new BASE64Decoder();

			try {

				return new String(decoder.decodeBuffer(cipher));

			} catch (IOException e) {

				// throw new InvalidImplementationException(

				// Fail

			}

		}

		return null;
	}

}
