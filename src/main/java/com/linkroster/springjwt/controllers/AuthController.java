package com.linkroster.springjwt.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
import java.nio.file.Files;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.mail.MessagingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


import com.linkroster.springjwt.models.ERole;

import com.linkroster.springjwt.models.Role;
import com.linkroster.springjwt.models.User;


import com.linkroster.springjwt.payload.request.IdContainer;
import com.linkroster.springjwt.payload.request.LoginRequest;
import com.linkroster.springjwt.payload.request.SignupRequest;
import com.linkroster.springjwt.payload.response.JwtResponse;
import com.linkroster.springjwt.payload.response.MessageResponse;

import com.linkroster.springjwt.repository.ImageRepository;

import com.linkroster.springjwt.payload.response.UserImageDto;


import com.linkroster.springjwt.repository.RoleRepository;
import com.linkroster.springjwt.repository.UserImageRepository;
import com.linkroster.springjwt.repository.UserRepository;
import com.linkroster.springjwt.security.jwt.JwtUtils;
import com.linkroster.springjwt.security.services.EmailService;
import com.linkroster.springjwt.security.services.SmsService;
import com.linkroster.springjwt.security.services.UserDetailsImpl;
import com.linkroster.springjwt.service.UserService;
import com.linkroster.springjwt.twiliosms.TwilioConfiguration;

import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/auth/")
public class AuthController {
	
	
	@Value("${spring.mail.username}")
	private String gmail;
	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired		
	UserService userService;
	@Autowired
	UserRepository userRepository;

	
	@Autowired
	ImageRepository imageRepository;
	
	

	
	@Autowired
	UserImageRepository userimageRepository;
	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	private JavaMailSender javaMailSender;
	@Autowired
	private EmailService emailService;
	@Autowired
	private SmsService service;

	@Autowired
	private TwilioConfiguration twilioConfiguration;
	
	

//
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest,
			HttpServletRequest request) {
		String Phoneregx = "^[0][1-9]\\d{9}$|^[1-9]\\d{9}$";
		String Emailregex = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}";

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getemail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtUtils.generateJwtToken(authentication);
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(),
				userDetails.getEmail(), roles, userDetails.getIsEmailVerified(), userDetails.getConfirmationToken(),
				userDetails.getIsPhoneVerified()));

	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest,

			HttpServletRequest request) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
		}

		if (userRepository.existsByphoneNumber(signUpRequest.getPhoneNumber())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: PhoneNumber is already in use!"));
		}
		// Create new user's account
		User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(), signUpRequest.getPassword(),
				signUpRequest.getPhoneNumber());

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);

		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "mod":
					Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}
		user.setPassword(encoder.encode(user.getPassword()));
		user.setRoles(roles);

		user.setIsEmailVerified("0");
		user.setIsPhoneVerified("0");
		// Generate random 36-character string token for confirmation link
		String otp = String.valueOf(new Random().nextInt(95509));
		user.setConfirmationToken(otp);

		//String appUrl = request.getScheme() + "://" + request.getServerName() ;
		String appUrl="http://linkroster.com";
		SimpleMailMessage registrationEmail = new SimpleMailMessage();
		registrationEmail.setTo(user.getEmail());
		registrationEmail.setSubject("Registration Confirmation");
		registrationEmail.setText("To confirm your e-mail address, please click the link below:\n" + appUrl
				+ "/confirm?resetToken=" + user.getConfirmationToken()
				+ "\nYou may be asked to enter this confirmation code\n" + user.getConfirmationToken());
//		registrationEmail.setText( "You may be asked to enter this confirmation code"
//				+user.getConfirmationToken());


		registrationEmail.setFrom("gmail");


		javaMailSender.send(registrationEmail);

//		service.sendSmsCode(signUpRequest.getPhoneNumber(), otp);

		user.setPhoneOTP(otp);
		userRepository.save(user);
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"
				+ " A confirmation e-mail has been sent to" + user.getEmail() + user.getPhoneOTP()));

	}

	@GetMapping("/confirm/{token}")
	public String showConfirmationPage(@PathVariable("token") String token) {

		User user = userRepository.findByConfirmationToken(token);
		if (user == null) { // No token found in DB
			return "invalidToken" + "Oops!  This is an invalid confirmation link.";
		} else { // Token found
			user.setIsEmailVerified("1");
			userRepository.save(user);
			return "Congratulations! Your account has been activated and email is verified! ";

		}

	}

	@GetMapping("/requestresetEmail/{email:.+}")
	public ResponseEntity<?> RequestResetEmail(@PathVariable("email") String email, HttpServletRequest request) {
		String Phoneregx = "^[0][1-9]\\d{9}$|^[1-9]\\d{9}$";
		String Emailregex = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}";
		String otp = String.valueOf(new Random().nextInt(95509));
		User user = new User();
		if (email.matches(Emailregex)) {

			user = userRepository.findByEmail(email).filter(c -> c.getIsEmailVerified().contains("1"))

					.orElseThrow(() -> new UsernameNotFoundException("User Not Found with EmailId: " + email));
			// Generate random 36-character string token for reset password
			user.setConfirmationToken(UUID.randomUUID().toString());
			// Save token to database
			userRepository.save(user);

			//String appUrl = request.getScheme() + "://" + request.getServerName() ;
			String appUrl="http://linkroster.com";
			SimpleMailMessage passwordResetEmail = new SimpleMailMessage();


			passwordResetEmail.setFrom("gmail");


			passwordResetEmail.setTo(user.getEmail());
			passwordResetEmail.setSubject("Password Reset Request");
			passwordResetEmail.setText("To reset your password, click the link below:\n" + appUrl
					+ "/changePassword?resetToken=" + user.getConfirmationToken());

			javaMailSender.send(passwordResetEmail);

			// return "A password reset link has been sent to " + user.getEmail();
			return ResponseEntity.ok(new MessageResponse("A password reset link has been sent to " + user.getEmail()));

		}

		else if (email.matches(Phoneregx))

			user = userRepository.findByphoneNumber(email).filter(c -> c.getIsPhoneVerified().contains("1"))

					.orElseThrow(() -> new UsernameNotFoundException("User Not Found with PhoneNumber: " + email));

		user.setPhoneOTP(otp);
		userRepository.save(user);
		//service.sendSmsCode(email, otp);

		return ResponseEntity.ok(new MessageResponse(otp));

	}

	public ResponseEntity<?> logimSmsOTP(@PathVariable("email") String email, HttpServletRequest request) {

		String otp = String.valueOf(new Random().nextInt(95509));
		User user = new User();

		String Phoneregx = "^[0][1-9]\\d{9}$|^[1-9]\\d{9}$";

		if (email.matches(Phoneregx))

			user = userRepository.findByphoneNumber(email).filter(c -> c.getIsPhoneVerified().contains("0"))

					.orElseThrow(() -> new UsernameNotFoundException("User Not Found with PhoneNumber: " + email));

		user.setPhoneOTP(otp);
		userRepository.save(user);
		//service.sendSmsCode(email, otp);

		return ResponseEntity.ok(new MessageResponse(otp));

	}
	@PutMapping("/findalluser/{id}")
	public ResponseEntity<?> updateuserbyid(@PathVariable("id") long id,
	@RequestBody User user) {
	User userfromDB;


	userfromDB = userRepository.findById(id).
	orElseThrow(() -> new
	UsernameNotFoundException("User Not Found with "

	+id ));

	if (userfromDB != null) {

	userfromDB.setUsername(user.getUsername());
	
	
	userfromDB.setEmail(user.getEmail());
	userfromDB.setPhoneNumber(user.getPhoneNumber());
	
	userRepository.save(userfromDB);

	return ResponseEntity.ok(new MessageResponse("user updated successfully!" +
	user.getUsername()+user.getEmail()+user.getPhoneNumber()));

	} else

	{
	return ResponseEntity.ok(new MessageResponse("error"));
	}
	}
//@GetMapping("/getUser/{username}")
//public ResponseEntity<List<User>> getUserSearch(@PathVariable("username") String username)
//{
//List<User> userList = userRepository.findByUsernameContainingIgnoreCase(username);
//return new ResponseEntity<List<User>>(userList,new HttpHeaders(),HttpStatus.OK);
//
//}
	@GetMapping("/getuserbyemail/{email:.+}")
	public String getUserbyemail(@PathVariable("email") String email, @RequestBody User user) {
		user = userRepository.findByEmail(email)

				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with EmailId: " + email));

		return "success";

	}

	@GetMapping("/getuserbytoken/{resettoken}")
	public ResponseEntity<User> getUserbytoken(@PathVariable("resettoken") String resettoken)

	{
		try {
			User user = userRepository.findByConfirmationToken(resettoken);
			if (user != null) {
				return new ResponseEntity<>(user, HttpStatus.OK);

			} else {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);

			}
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);

		}

	}

	@GetMapping("/getUserbyOTP/{OTP}")
	public ResponseEntity<?> getUserbyOTP(@PathVariable("OTP") String phoneOTPcode)

	{
		try {
			User user = userRepository.findByphoneOTPcode(phoneOTPcode);

			if (user != null) {
				user.setIsPhoneVerified("1");
				userRepository.save(user);
				return ResponseEntity.ok(new MessageResponse("Code is Verified You may login"));

			} else {
				return ResponseEntity.ok(new MessageResponse("Please try again it is not valid code"));

			}
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);

		}

	}

	@PostMapping("/updatepassword/{resetToken}")
	public ResponseEntity<?> updatepassword(@PathVariable("resetToken") String resettoken, @RequestBody User user) {
		User userfromDB;
		String otpTokenregex = "^[0][1-9]\\d{9}$|^[1-9]\\d{9}$";
		if (resettoken.matches(otpTokenregex)) {

			userfromDB = userRepository.findByphoneNumber(resettoken).orElseThrow(
					() -> new UsernameNotFoundException("User Not Found with " + "PhoneNumber: " + resettoken));
		} else

			userfromDB = userRepository.findByConfirmationToken(resettoken);

		if (userfromDB != null) {

			userfromDB.setPassword(encoder.encode(user.getPassword()));

			userRepository.save(userfromDB);
			return ResponseEntity.ok(new MessageResponse("Password updated successfully!" + user.getPassword()));

		} else

		{
			return ResponseEntity.ok(new MessageResponse("error"));
		}
	}

	@GetMapping("/findall")
	public List<User> getAllUser() {
		List<User> userResponse = (List<User>) userRepository.findAll();
		return userResponse;
	}
//	@GetMapping("/testing/{userid}")
//	public List<UserImageDto> test(@PathVariable("userid") long userid) {
//		List<UserImageDto> userResponse = (List<UserImageDto>) userRepository.getUserInfo(userid);
//		return userResponse;
//	}
	@PostMapping(value = "/findallbyid")
	public List<User> getAllUserlist(@RequestBody IdContainer[] idContainer) {

		List<Long> ids = new ArrayList();
		for (IdContainer id : idContainer) {
			ids.add(id.getChainuserid());
		}

		List<User> userResponse = (List<User>) userRepository.findAllById(ids);
//		List<ImageModel> image = (List<ImageModel>)imageRepository.findAllById(ids);
		
		return userResponse;
	}
	
	@PostMapping(value = "/findbyUserImage")
	public List<UserImageDto> getUserImageList(@RequestBody IdContainer[] idContainer) {
		String s="";
		List<Long> ids = new ArrayList();
		for (IdContainer id : idContainer) {
			ids.add(id.getChainuserid());
		}
		
		List<UserImageDto> userimageInfo=new ArrayList();
		
		
		List<UserImageDto> userResponse = (List<UserImageDto>) userRepository.getUserInfo(ids);
		
        
	return userResponse;
	}
	
	@PostMapping(value = "/findallbyemailid")
	public List<User>  findallbyemailid(@RequestBody IdContainer[] idContainer) {

		List<String> emailids = new ArrayList();
		for (IdContainer emailid : idContainer) {
			emailids.add(emailid.getInviteEmailid());
		}
		List<User> userResponse = (List<User>) userRepository.getuser(emailids);
		
		return userResponse;
		
	}


	@PostMapping("/updateuser/{userid}")
	public ResponseEntity<?> updateuser(@PathVariable("userid") long userid, @RequestBody User user) {
		User userfromDB;

		userfromDB = userRepository.findById(userid)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with "

						+ userid));
	
		if (userfromDB != null) {

			userfromDB.setUsername(user.getUsername());
			userRepository.save(userfromDB);

			return ResponseEntity.ok(new MessageResponse("user updated successfully!" + user.getUsername()));

		} else

		{
			return ResponseEntity.ok(new MessageResponse("error"));
		}
	}
	
	
	
	
	@PutMapping("/updateduser/{id}")
	public ResponseEntity<?> updateusers(@PathVariable("id") long id,
			@RequestBody User user) {
		User userfromDB;
//		ImageModel imagefromDB;
			
			 userfromDB = userRepository.findById(id).
					 orElseThrow(() -> new
							 UsernameNotFoundException("User Not Found with "
					 		 
						+id ));
//			 imagefromDB= imageRepository.findByUserId(userId);
					
		if (userfromDB != null) {

			userfromDB.setUsername(user.getUsername());
			userfromDB.setEmail(user.getEmail());
			userfromDB.setPhoneNumber(user.getPhoneNumber());
//			imagefromDB.setName(image.getName());
//			imagefromDB.setPicByte(image.getPicByte());
//			imagefromDB.setType(image.getType());
			userRepository.save(userfromDB);
//			imageRepository.save(imagefromDB);
			
			return ResponseEntity.ok(new MessageResponse("user updated successfully!" +
			user.getUsername()+user.getEmail()+user.getPhoneNumber()
//			+image.getName()+
//			image.getType()+
//			image.getPicByte()
			));

		} else

		{
			return ResponseEntity.ok(new MessageResponse("error"));
		}
	}
	

	
	@GetMapping("/getUserListing/{username}/{id}")
	public ResponseEntity<List<User>> getUser(@PathVariable("username")String username,@PathVariable("id")long id)
	{
		
//		List<User> datauserList = userRepository.findAll().
//				stream().filter(s->s.getId()!=id ).collect(Collectors.toList());
		List<User> userList = userRepository.findByUsernameContainingIgnoreCase(username).  
		stream().filter(s->s.getId()!=id ).collect(Collectors.toList());

//		 List<User> combinedList = Stream.of(datauserList, userList)
//                 .flatMap(x -> x.stream())
//                 .collect(Collectors.toList());
		
		return new ResponseEntity<List<User>>(userList,new HttpHeaders(),HttpStatus.OK);
		
	}
	
	

	@GetMapping("/getUsers/{id}")
	public ResponseEntity<List<User>> getUser(@PathVariable("id")long id)
	{
		
		List<User> userList = userRepository.findByid(id);
		
				
		return new ResponseEntity<List<User>>(userList,new HttpHeaders(),HttpStatus.OK);
		
	}
	
	
	
	@GetMapping("/finduserlist")
	public List<User> getAllUsers() {
		List<User> userResponse = (List<User>) userRepository.findAll();
		
		
		 if (userResponse == null) {
    throw new UsernameNotFoundException("Could not find user with that email");
}
		 return userResponse;
	}

	
@GetMapping("/getUserlist/{id}")
public List<UserImageDto> getUserlist(@PathVariable long id){
	return userService.getAllUsers(id);
}




@GetMapping("/getUsername")
public List<User> getUsername(){
return userService.getAllUser();
}




}
