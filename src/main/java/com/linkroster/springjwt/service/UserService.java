package com.linkroster.springjwt.service;

import java.util.List;
import java.util.stream.Collectors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.hibernate.validator.internal.util.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;



import com.linkroster.springjwt.models.User;


import com.linkroster.springjwt.payload.response.UserImageDto;

import com.linkroster.springjwt.repository.ImageRepository;

import com.linkroster.springjwt.repository.UserRepository;



@Service
public class UserService {
	
	
private UserRepository userRepository;

private ImageRepository imageRepository;


	
    @Autowired
    public UserService(UserRepository userRepository) { 
      this.userRepository = userRepository;
    }


	public User findByConfirmationToken(String confirmationToken) {
		return userRepository.findByConfirmationToken(confirmationToken);
	}

	public List<User> getAllUser() {
		List<User> userResponse = (List<User>) userRepository.findAll();
				
		
		return userResponse;
	}
	
	
		public List<UserImageDto> getAllUsers (long id){
		
		List<UserImageDto> userResponse = (List<UserImageDto>) userRepository.getUserlist(id).
				stream().filter(user -> user.getIsEmailVerified().contains("1"))
				
				.collect(Collectors.toList());
	
		return userResponse;
		
//		return getAllUser().stream().filter(user -> user.getId()!= id).collect(Collectors.toList());
	}
	
		
		
	
}
