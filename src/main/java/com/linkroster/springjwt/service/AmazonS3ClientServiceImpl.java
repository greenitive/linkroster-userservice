package com.linkroster.springjwt.service;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.linkroster.springjwt.models.UserImage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Random;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;


@Component
public class AmazonS3ClientServiceImpl implements com.linkroster.springjwt.repository.AmazonS3ClientService 
{
private String awsS3AudioBucket;
private AmazonS3 amazonS3;
private static Random rand = new Random((new Date()).getTime());
private static final Logger logger = LoggerFactory.getLogger(AmazonS3ClientServiceImpl.class);
@Autowired
public AmazonS3ClientServiceImpl(Region awsRegion, AWSCredentialsProvider awsCredentialsProvider, String awsS3AudioBucket) 
    {
this.amazonS3 = AmazonS3ClientBuilder.standard()
                .withCredentials(awsCredentialsProvider)
                .withRegion(awsRegion.getName()).build();
this.awsS3AudioBucket = awsS3AudioBucket;
    }
public static String encrypt(String str) {
	 
	  BASE64Encoder encoder = new BASE64Encoder();
	 
	  byte[] salt = new byte[8];
	 
	  rand.nextBytes(salt);
	 
	  return encoder.encode(salt) + encoder.encode(str.getBytes());
	    }
public static String decrypt(String encstr) {
	 
	  if (encstr.length() > 12) {
	 
	String cipher = encstr.substring(12);
	 
	BASE64Decoder decoder = new BASE64Decoder();
	 
	try {
	 
	    return new String(decoder.decodeBuffer(cipher));
	 
	} catch (IOException e) {
	 
	    //  throw new InvalidImplementationException(
	 
	    //Fail
	 
	}
	 
	  }
	 
	  return null;
	    }


@Async
public void uploadFileToS3Bucket(MultipartFile multipartFile, boolean enablePublicReadAccess) 
    {
	
	
String fileName = multipartFile.getOriginalFilename();
String enc = fileName;

System.out.println("Encrypted string :" + enc);

System.out.println("Decrypted string :" + decrypt(enc));
try {
//creating the file in the server (temporarily)
File file = new File(fileName);
FileOutputStream fos = new FileOutputStream(file);
            fos.write(multipartFile.getBytes());
            fos.close();
PutObjectRequest putObjectRequest = new PutObjectRequest(this.awsS3AudioBucket, enc, file);
if (enablePublicReadAccess) {
                putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
            }
this.amazonS3.putObject(putObjectRequest);
//removing the file created in the server
            //file.delete();


//img.setUserId(userId);

        } catch (IOException | AmazonServiceException ex) {
            logger.error("error [" + ex.getMessage() + "] occurred while uploading [" + fileName + "] ");
        }
    }
@Async
public void deleteFileFromS3Bucket(String fileName) 
    {
try {
            amazonS3.deleteObject(new DeleteObjectRequest(awsS3AudioBucket, fileName));
        } catch (AmazonServiceException ex) {
            logger.error("error [" + ex.getMessage() + "] occurred while removing [" + fileName + "] ");
        }
    }
}