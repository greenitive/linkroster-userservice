package com.linkroster.springjwt.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.linkroster.springjwt.models.ImageModel;





public interface ImageRepository extends JpaRepository<ImageModel, Long> {
	ImageModel findByUserId(Long userId);

//	ImageModel save(Optional<ImageModel> retrievedImage);

//	void save(Optional<ImageModel> retrievedImage);
}
