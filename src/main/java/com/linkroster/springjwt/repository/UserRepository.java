package com.linkroster.springjwt.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.linkroster.springjwt.models.Role;
import com.linkroster.springjwt.models.User;
import com.linkroster.springjwt.payload.response.UserImageDto;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
//	Optional<User> findByUsername(String username);
	
	Optional<User> findByEmail(String email);
	@Query("SELECT u FROM User u WHERE u.email IN :ids")
	List<User> getuser(List<String> ids);
	@Query("SELECT u FROM User u WHERE u.id IN :ids")
	List<User> getuserid(List<Long> ids);
	//findbyEmailIn(Collection<Age> ages)
	Optional<User> findByphoneNumber(String phoneNumber);
	Boolean existsByUsername(String username);
	Boolean existsByphoneNumber(String phoneNumber);
	
		Boolean existsByEmail(String email);
	
	List<User> findByEmailContainingIgnoreCase(String email);
	List<User> findByPhoneNumber(String phoneNumber);
		
	User findByphoneOTPcode(String phoneOTPcode);
	
    User findByConfirmationToken(String confirmationToken);
    @Query("select new com.linkroster.springjwt.payload.response.UserImageDto"
			+ "(u.id,u.username,u.email,u.IsEmailVerified,ui.name"
	+ ")"
	+ " from User u "
	
	+" Left join u.userImage ui"
	+ " where u.id IN :ids ")
	List<UserImageDto> getUserInfo(List<Long> ids);

	
	Optional<Role> findById(int userId);
//	Collection<User> findByUsername(String username);
	User findUserById(long id);

	User getUserByEmail(String email);

//	@Query(value="SELECT A.id, A.email,A.password, A.phone_number,A.username, B.name, B.pic_byte,B.type FROM users AS A	LEFT JOIN user_image AS B ON B.id = A.id", nativeQuery=true)
	
	 @Query("select new com.linkroster.springjwt.payload.response.UserImageDto"
				+ "(u.id,u.username,u.email,u.IsEmailVerified,ui.name"
		+ ")"
		+ " from User u "
		
		+" Left join u.userImage ui"
		+ " where u.id NOT IN (:id)")
	List<UserImageDto> getUserlist(long id);
	 
	
	 List<User> findByid(long id);
	  @Query("select new com.linkroster.springjwt.payload.response.UserImageDto"
				+ "(u.id,u.username,u.email,u.IsEmailVerified,ui.name"
		+ ")"
		+ " from User u "
		
		+" Left join u.userImage ui"
		+ " where u.id IN :id")
	 List<UserImageDto>getUsers(long id);
//	@Query("SELECT users(d.name, e.password, e.email, e.phone_number) "
//			+ "FROM users d LEFT JOIN d.user_image e")
//	List<User> fetchEmpDeptDataLeftJoin();
	
//	  @Query("SELECT u FROM User u WHERE u.id,u.username  IN (:id,:username)")
	  
	 
	  List<User> findByUsernameContainingIgnoreCase(String username);
	
}