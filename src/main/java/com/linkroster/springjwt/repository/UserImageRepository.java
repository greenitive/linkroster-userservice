package com.linkroster.springjwt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.linkroster.springjwt.models.ImageModel;
import com.linkroster.springjwt.models.User;
import com.linkroster.springjwt.models.UserImage;
import com.linkroster.springjwt.payload.response.UserImageDto;

@Repository
public interface UserImageRepository extends JpaRepository<UserImage, Long> {
	 @Query("select new com.linkroster.springjwt.payload.response.UserImageDto"
				+ "(u.id"
		+ ")"
		+ " from User u "
		
		//+" inner join u.userImage ui"
		+ " where  u.id=:userId ")
		List<UserImageDto> getUserInfo(long userId);
	 UserImage findByUserId(Long userId);
}
