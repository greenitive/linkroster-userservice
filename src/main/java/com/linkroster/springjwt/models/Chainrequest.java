package com.linkroster.springjwt.models;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Chainrequest {
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	

	@Column(name = "userid")
	private int userid;
	

	@Column(name = "chainuserid")
	private int chainuserid;

	@Column(name = "status")
	private boolean status;

	

	public Chainrequest() {
	}
	

	
	public Chainrequest(long id,int userid,int chainuserid, boolean status) {
		super();
		this.id = id;
		this.userid= userid;
		
		this.chainuserid = chainuserid;
		this.status = status;
	}



	public int getUserid() {
		return userid;
	}



	public void setUserid(int userid) {
		this.userid = userid;
	}



	public int getChainuserid() {
		return chainuserid;
	}



	public void setChainuserid(int chainuserid) {
		this.chainuserid = chainuserid;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}





	public boolean isStatus() {
		return status;
	}



	public void setStatus(boolean status) {
		this.status = status;
	}





	@Override
	public String toString() {
		return "Chainrequest [id=" + id + ", userid=" + userid + ", chainuserid=" + chainuserid + ", status=" + status
				+ "]";
	}











}
