package com.linkroster.springjwt.models;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Entity
@Table(name = "users", uniqueConstraints = { @UniqueConstraint(columnNames = "username"),
		@UniqueConstraint(columnNames = "email") })
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(max = 20)
	private String username;

	@NotBlank
	@Size(max = 50)
	@Email
	private String email;

	@NotBlank
	@Size(max = 120)
	private String password;
	/* Addded for confirmation email */
//	@Column(name = "enabled")
//	private boolean enabled;

	@NotBlank
	@Size(max = 120)
	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "confirmation_token")
	private String confirmationToken;

	@Column(name = "phoneOTPcode")
	private String phoneOTPcode;

	@Column(name = "isemailverified")
	private String IsEmailVerified;
	@Column(name = "isphoneverified")
	private String IsPhoneVerified;
	
	@Column(name = "FriendStatus")
	private String IsFriendStatus;

	public String getIsFriendStatus() {
		return IsFriendStatus;
	}

	public void setIsFriendStatus(String isFriendStatus) {
		IsFriendStatus = isFriendStatus;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"),
	inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Role> roles = new HashSet<>();
	
//	@ManyToMany(fetch = FetchType.LAZY)
//	@JoinTable(name = "bookmark_category_id", joinColumns = @JoinColumn(name = "user_id"), 
//	inverseJoinColumns = @JoinColumn(name = "bookmark_category_id"))
//	private Set<BookmarkCategory> BookmarkCategory = new HashSet<>();
	 /*mapped to TeamBookmark -->team Parent Table */
		@OneToMany(fetch=FetchType.LAZY, mappedBy="user")   
		private List<UserImage> userImage;
		
	public User() {
	}

	public User(String username, String email, String password, String phoneNumber) {
		this.username = username;
		this.email = email;
		this.password = password;
		this.phoneNumber = phoneNumber;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

//	public boolean getEnabled() {
//		return enabled;
//	}
//
//	public void setEnabled(boolean value) {
//		this.enabled = value;
//	}

	public String getConfirmationToken() {
		return confirmationToken;
	}

	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken =confirmationToken;
	}

	public String getPhoneOTP() {
		return phoneOTPcode;
	}

	public void setPhoneOTP(String phoneOTPcode) {
		this.phoneOTPcode = phoneOTPcode;
	}
	public String getIsEmailVerified() {
		return IsEmailVerified;
	}

	public void setIsEmailVerified(String IsEmailVerified) {
		this.IsEmailVerified = IsEmailVerified;
	}

	public String getIsPhoneVerified() {
		return IsPhoneVerified;
	}

	public void setIsPhoneVerified(String IsPhoneVerified) {
		this.IsPhoneVerified = IsPhoneVerified;
	}
}
