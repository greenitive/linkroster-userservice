
package com.linkroster.springjwt.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "UserImage",uniqueConstraints = { @UniqueConstraint(columnNames = "imageid")})
public class UserImage {


	@Id
	@Column(name = "imageid")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long imageid;
//	@Column(name= "userId")
//	private Long userId;
//	public Long getUserId() {
//		return userId;
//	}

//	public void setUserId(Long userId) {
//		this.userId = userId;
//	}

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private String type;

    //image bytes can have large lengths so we specify a value
    //which is more than the default length for picByte column
	@Lob
	@Column(name = "picByte", length = 1000)
	private byte[] picByte;
	/*Team Id is foreign key*/
	 @ManyToOne(cascade = CascadeType.ALL)
	 @JoinColumn(name="id")
	 private User user;
	 
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte[] getPicByte() {
		return picByte;
	}

	public void setPicByte(byte[] picByte) {
		this.picByte = picByte;
	}

	public UserImage(Long imageid, String name, String type, byte[] picByte) {
		super();
		this.imageid = imageid;
		this.name = name;
		this.type = type;
		this.picByte = picByte;
		
	}
	public UserImage(User userid, String name, String type) {
		super();
		this.user = userid;
		this.name = name;
		this.type = type;
		
		
	}
	public UserImage()
	{
		super();
	}
}
