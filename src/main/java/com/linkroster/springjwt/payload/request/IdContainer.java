package com.linkroster.springjwt.payload.request;


public class IdContainer {
	Long chainuserid;
	String inviteEmailid;

	

	public String getInviteEmailid() {
		return inviteEmailid;
	}

	public void setInviteEmailid(String inviteEmailid) {
		this.inviteEmailid = inviteEmailid;
	}

	public Long getChainuserid() {
		return chainuserid;
	}

	public void setChainuserid(Long chainuserid) {
		this.chainuserid = chainuserid;
	}
 
	
}