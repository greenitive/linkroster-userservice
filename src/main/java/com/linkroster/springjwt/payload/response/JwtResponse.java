package com.linkroster.springjwt.payload.response;

import java.util.List;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private Long id;
	private String username;
	private String email;
	private List<String> roles;
	private String IsEmailVerified;
	private String IsPhoneVerified;
	private String ConfirmationToken;
	

	public JwtResponse(String accessToken, Long id, String username, String email, 
			List<String> roles,
			String IsEmailVerified,String ConfirmationToken,String IsPhoneVerified)
			{
		this.token = accessToken;
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
		this.IsEmailVerified=IsEmailVerified;
		this.ConfirmationToken=ConfirmationToken;
		this.IsPhoneVerified=IsPhoneVerified;
		
	}
	public String getIsPhoneVerified() {
		return IsPhoneVerified;
	}

	public void setIsPhoneVerified(String IsPhoneVerified) {
		this.IsPhoneVerified = IsPhoneVerified;
	}
	
	public String getConfirmationToken() {
		return ConfirmationToken;
	}

	public void setConfirmationToken(String ConfirmationToken) {
		this.ConfirmationToken = ConfirmationToken;
	}
	
	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getRoles() {
		return roles;
	}
	public String getIsEmailVerified() {
		return IsEmailVerified;
	}

	public void setIsEmailVerified(String IsEmailVerified) {
		this.IsEmailVerified = IsEmailVerified;
	}
}
