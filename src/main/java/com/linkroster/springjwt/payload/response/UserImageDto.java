package com.linkroster.springjwt.payload.response;



public class UserImageDto {

	

	public UserImageDto(Long id, String username, String email, String isEmailVerified, String name) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.IsEmailVerified = isEmailVerified;
		this.name = name;
		
//		this.picByte = picByte;
//		this.type = type;
	}

	



	public UserImageDto(Long id, String username, String email, String phoneNumber, String isEmailVerified, String name,
			byte[] picByte, String type) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.IsEmailVerified = isEmailVerified;
		this.name = name;
		this.picByte = picByte;
		this.type = type;
	}





	private Long id;
	
	private String username;
	private String email;
	private String phoneNumber;
	private String IsEmailVerified;
	
	private String name;
	private byte[] picByte;
	private String type;
	
//	public UserImageDto(Long Id, String name, byte[] picByte, String type) {
//		super();
//		this.id = Id;
//		this.name = name;
//		this.picByte = picByte;
//		this.type = type;
//	}
//
//	public UserImageDto(String username, String name) {
//		
//		this.username = username;
//		this.name = name;
//	}

	
	public Long getId() {
		return id;
	}

	
	public String getIsEmailVerified() {
		return IsEmailVerified;
	}


	public void setIsEmailVerified(String isEmailVerified) {
		this.IsEmailVerified = isEmailVerified;
	}


	public UserImageDto(Long id) {
		super();
		this.id = id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getPicByte() {
		return picByte;
	}

	public void setPicByte(byte[] picByte) {
		this.picByte = picByte;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	
}
