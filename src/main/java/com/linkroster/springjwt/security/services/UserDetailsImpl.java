package com.linkroster.springjwt.security.services;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.linkroster.springjwt.models.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserDetailsImpl implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String username;

	private String email;

	@JsonIgnore
	private String password;
	private String phoneNumber;
	private String IsEmailVerified;
	private String IsPhoneVerified;
	private String ConfirmationToken;

	private Collection<? extends GrantedAuthority> authorities;

	public UserDetailsImpl(Long id, String username, String email, String password,
			Collection<? extends GrantedAuthority> authorities,String phoneNumber,
			String IsEmailVerified,String IsPhoneVerified,String ConfirmationToken) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.phoneNumber=phoneNumber;
		
		this.authorities = authorities;
		this.IsEmailVerified=IsEmailVerified;
		
		this.ConfirmationToken=ConfirmationToken;
		this.IsPhoneVerified=IsPhoneVerified;
		
	}

	public static UserDetailsImpl build(User user) {
		List<GrantedAuthority> authorities = user.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getName().name()))
				
				.collect(Collectors.toList());

		return new UserDetailsImpl(
				user.getId(), 
				user.getUsername(), 
				user.getEmail(),
				user.getPassword(), 
				
				authorities,
				user.getPhoneNumber(),
				user.getIsEmailVerified(),
				user.getIsPhoneVerified(),
				user.getConfirmationToken());
		
		
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public String getConfirmationToken() {
		return ConfirmationToken;
	}
	public String getIsPhoneVerified() {
		return IsPhoneVerified;
	}
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}


	public String getIsEmailVerified() {
		return IsEmailVerified;
	}

	public void setIsEmailVerified(String IsEmailVerified) {
		this.IsEmailVerified = IsEmailVerified;
	}

	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserDetailsImpl user = (UserDetailsImpl) o;
		return Objects.equals(id, user.id);
	}
}
