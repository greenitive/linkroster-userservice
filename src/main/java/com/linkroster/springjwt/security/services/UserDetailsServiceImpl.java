package com.linkroster.springjwt.security.services;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.linkroster.springjwt.models.User;
import com.linkroster.springjwt.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	UserRepository userRepository;
	

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		String Phoneregx="^[0][1-9]\\d{9}$|^[1-9]\\d{9}$";
		String Emailregex="[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}";
		User user=new User();
		if(username.matches(Emailregex))
		{
		 user = userRepository.findByEmail(username)
				//.filter(c -> c.getIsEmailVerified()=="1")
				
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with EmailId: " + 
				username));
		
		
		}
		else if(username.matches(Phoneregx))
		{
			 user = userRepository.findByphoneNumber(username)
				
			///.filter(c -> c.getIsPhoneVerified().contains("1"))
					
					.orElseThrow(() -> new UsernameNotFoundException("User Not Found with PhoneNumber: " + username));

			
		}
		return UserDetailsImpl.build(user);
	}

}
