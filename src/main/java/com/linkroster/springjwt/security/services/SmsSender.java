package com.linkroster.springjwt.security.services;
import com.linkroster.springjwt.payload.request.SignupRequest;
import com.linkroster.springjwt.payload.request.SmsRequest;
import com.linkroster.springjwt.twiliosms.TwilioConfiguration;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;

import java.util.Random;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("twilio")
public class SmsSender {
	
	    private static final Logger LOGGER = LoggerFactory.getLogger(SmsSender.class);
	    
	    @Autowired
	    private final TwilioConfiguration twilioConfiguration;

	    
	    public SmsSender(TwilioConfiguration twilioConfiguration) {
	        this.twilioConfiguration = twilioConfiguration;
	    }
	    public void sendSmsCode(String ToPhoneNumber,String OTP) {
	    	  	
		        if (isPhoneNumberValid(ToPhoneNumber)) {
		            PhoneNumber to = new PhoneNumber(ToPhoneNumber);
		            PhoneNumber from = new PhoneNumber(twilioConfiguration.getTrialNumber());
		            String message ="your OTP to activate account"+OTP;
		            MessageCreator creator = Message.creator(to, from, message);
		            creator.create();
		            LOGGER.info("Send sms {}");
		        } else {
		            throw new IllegalArgumentException(
		                    "Phone number [" +ToPhoneNumber + "] is not a valid number"
		            );
		        }

		    }

	  
	    public void sendSms(SignupRequest signupRequest) {
    	//String otp=signupRequest.setPhoneOTP(String.valueOf(new Random().nextInt(95509)));
	    	
	    	signupRequest.setPhoneOTP();
	    	
	        if (isPhoneNumberValid(signupRequest.getPhoneNumber())) {
	            PhoneNumber to = new PhoneNumber(signupRequest.getPhoneNumber());
	            PhoneNumber from = new PhoneNumber(twilioConfiguration.getTrialNumber());
	            String message ="your OTP to activate account"+signupRequest.getPhoneOTP();
	            MessageCreator creator = Message.creator(to, from, message);
	            creator.create();
	            LOGGER.info("Send sms {}", signupRequest);
	        } else {
	            throw new IllegalArgumentException(
	                    "Phone number [" + signupRequest.getPhoneNumber() + "] is not a valid number"
	            );
	        }

	    }

	    private boolean isPhoneNumberValid(String phoneNumber) {
	        // TODO: Implement phone number validator
	        return true;
	    }
	    private String generateOTP() {
			return String.valueOf(new Random().nextInt(95509));
		}

}
