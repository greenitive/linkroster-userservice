package com.linkroster.springjwt.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.linkroster.springjwt.payload.request.SignupRequest;
import com.linkroster.springjwt.payload.request.SmsRequest;


@Service
public class SmsService {
	
	 private final SmsSender smsSender;

	    @Autowired
	    public SmsService(@Qualifier("twilio") SmsSender smsSender) {
	        this.smsSender = smsSender;
	    }

	    public void sendSms(SignupRequest  signupRequest) {
	        smsSender.sendSms(signupRequest);
	    }
	    
	    public void sendSmsCode(String ToPhoneNumber,String OTP) {
	        smsSender.sendSmsCode(ToPhoneNumber, OTP);
	    }
}
